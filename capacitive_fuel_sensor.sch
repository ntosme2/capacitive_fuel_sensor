EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FDC2214QRGHT:FDC2214QRGHT IC1
U 1 1 5FB997B0
P 6400 3200
F 0 "IC1" H 6550 3500 50  0000 R CNN
F 1 "FDC2214QRGHT" H 6550 3400 50  0000 R CNN
F 2 "user-footprints:FDC2214QRGHT" H 7450 3700 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/FDC2214-Q1" H 7450 3600 50  0001 L CNN
F 4 "4-channel, Noise-immune, AEC-Q100 qualified, 28-bit Capacitive Sensing Solution" H 7450 3500 50  0001 L CNN "Description"
F 5 "0.8" H 7450 3400 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 7450 3300 50  0001 L CNN "Manufacturer_Name"
F 7 "FDC2214QRGHTQ1" H 7450 3200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "FDC2214QRGHTQ1" H 7450 3100 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/fdc2214qrghtq1/texas-instruments" H 7450 3000 50  0001 L CNN "Arrow Price/Stock"
F 10 "FDC2214QRGHT" H 7450 2900 50  0001 L CNN "Mouser Part Number"
F 11 "FDC2214QRGHT" H 7450 2800 50  0001 L CNN "Mouser Price/Stock"
	1    6400 3200
	-1   0    0    1   
$EndComp
$Comp
L ATMEGA328P-AU:ATMEGA328P-AU IC2
U 1 1 5FB99F83
P 2050 5600
F 0 "IC2" H 3900 4000 50  0000 L CNN
F 1 "ATMEGA328P-AU" H 3900 4100 50  0000 L CNN
F 2 "user-footprints:ATMEGA328P-AU" H 3900 6800 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ATMEGA328P-AU.pdf" H 3900 6700 50  0001 L CNN
F 4 "MICROCHIP - ATMEGA328P-AU. - MICROCONTROLLER MCU, 8 BIT, ATMEGA, 20MHZ, TQFP-32" H 3900 6600 50  0001 L CNN "Description"
F 5 "1.2" H 3900 6500 50  0001 L CNN "Height"
F 6 "Microchip" H 3900 6400 50  0001 L CNN "Manufacturer_Name"
F 7 "ATMEGA328P-AU" H 3900 6300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "ATMEGA328P-AU" H 3900 6200 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/atmega328p-au/microchip-technology" H 3900 6100 50  0001 L CNN "Arrow Price/Stock"
F 10 "ATMEGA328P-AU" H 3900 6000 50  0001 L CNN "Mouser Part Number"
F 11 "ATMEGA328P-AU" H 3900 5900 50  0001 L CNN "Mouser Price/Stock"
	1    2050 5600
	1    0    0    -1  
$EndComp
$Comp
L MAX3221CPWR:MAX3221CPWR IC4
U 1 1 5FB9ABDE
P 8600 3550
F 0 "IC4" H 9250 3815 50  0000 C CNN
F 1 "MAX3221CPWR" H 9250 3724 50  0000 C CNN
F 2 "user-footprints:MAX3221CPWR" H 9750 3650 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/max3221" H 9750 3550 50  0001 L CNN
F 4 "3-V to 5.5-V Single-Channel RS-232 Line Driver/Receiver" H 9750 3450 50  0001 L CNN "Description"
F 5 "1.2" H 9750 3350 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 9750 3250 50  0001 L CNN "Manufacturer_Name"
F 7 "MAX3221CPWR" H 9750 3150 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "MAX3221CPWR" H 9750 3050 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/max3221cpwr/texas-instruments" H 9750 2950 50  0001 L CNN "Arrow Price/Stock"
F 10 "MAX3221CPWR" H 9750 2850 50  0001 L CNN "Mouser Part Number"
F 11 "MAX3221CPWR" H 9750 2750 50  0001 L CNN "Mouser Price/Stock"
	1    8600 3550
	1    0    0    -1  
$EndComp
$Comp
L TPS560430X3FDBVR:TPS560430X3FDBVR IC3
U 1 1 5FB9B1CD
P 6950 1050
F 0 "IC3" H 7450 1315 50  0000 C CNN
F 1 "TPS560430X3FDBVR" H 7450 1224 50  0000 C CNN
F 2 "user-footprints:TPS560430X3FDBVR" H 7800 1150 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tps560430" H 7800 1050 50  0001 L CNN
F 4 "SIMPLE SWITCHER 36-V, 600-mA Buck Regulator With High-Efficiency Sleep Mode" H 7800 950 50  0001 L CNN "Description"
F 5 "1.45" H 7800 850 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 7800 750 50  0001 L CNN "Manufacturer_Name"
F 7 "TPS560430X3FDBVR" H 7800 650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "TPS560430X3FDBVR" H 7800 550 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/tps560430x3fdbvr/texas-instruments" H 7800 450 50  0001 L CNN "Arrow Price/Stock"
F 10 "TPS560430X3FDBVR" H 7800 350 50  0001 L CNN "Mouser Part Number"
F 11 "TPS560430X3FDBVR" H 7800 250 50  0001 L CNN "Mouser Price/Stock"
	1    6950 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5FB9C237
P 8300 1350
F 0 "C9" H 8415 1396 50  0000 L CNN
F 1 "2.2uF" H 8415 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8338 1200 50  0001 C CNN
F 3 "~" H 8300 1350 50  0001 C CNN
	1    8300 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5FB9D0C9
P 8300 800
F 0 "C8" H 8415 846 50  0000 L CNN
F 1 "0.1uF" H 8415 755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8338 650 50  0001 C CNN
F 3 "~" H 8300 800 50  0001 C CNN
	1    8300 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5FB9D63C
P 8850 1250
F 0 "C10" H 8965 1296 50  0000 L CNN
F 1 "22uF" H 8965 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8888 1100 50  0001 C CNN
F 3 "~" H 8850 1250 50  0001 C CNN
	1    8850 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L1
U 1 1 5FB9DE06
P 8700 1050
F 0 "L1" V 8885 1050 50  0000 C CNN
F 1 "12uH" V 8794 1050 50  0000 C CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 8700 1050 50  0001 C CNN
F 3 "~" H 8700 1050 50  0001 C CNN
	1    8700 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 1150 8000 1150
Wire Wire Line
	8000 1150 8000 1250
Wire Wire Line
	8000 1250 7950 1250
Wire Wire Line
	9350 2350 9500 2350
Text Label 9350 2350 2    50   ~ 0
TX_232
Wire Wire Line
	9350 2450 9500 2450
Text Label 9350 2450 2    50   ~ 0
RX_232
Wire Wire Line
	9350 2150 9500 2150
Wire Wire Line
	10650 2250 10500 2250
Text Label 9350 2150 2    50   ~ 0
GND
Text Label 10650 2250 0    50   ~ 0
VIN
Wire Wire Line
	6800 1150 6950 1150
Text Label 6800 1150 0    50   ~ 0
GND
Text Label 8150 1150 2    50   ~ 0
VIN
Connection ~ 8000 1150
Text Label 8300 1550 0    50   ~ 0
GND
Wire Wire Line
	8300 1150 8300 1200
Wire Wire Line
	8000 1150 8300 1150
Wire Wire Line
	8300 1550 8300 1500
Wire Wire Line
	6950 1050 6900 1050
Wire Wire Line
	6900 1050 6900 650 
Wire Wire Line
	6900 650  8300 650 
Wire Wire Line
	7950 1050 8300 1050
Wire Wire Line
	8300 1050 8300 950 
Wire Wire Line
	8600 1050 8300 1050
Connection ~ 8300 1050
Wire Wire Line
	8800 1050 8850 1050
Wire Wire Line
	8850 1050 8850 1100
Text Label 8850 1550 0    50   ~ 0
GND
Wire Wire Line
	8850 1400 8850 1550
Text Label 8850 1050 0    50   ~ 0
3.3V
Wire Wire Line
	1950 5900 2050 5900
Text Label 1950 5900 2    50   ~ 0
3.3V
Text Label 1950 6100 2    50   ~ 0
3.3V
Wire Wire Line
	4150 6200 4050 6200
Text Label 4150 6200 0    50   ~ 0
3.3V
$Comp
L Device:C C3
U 1 1 5FBF9225
P 1100 6250
F 0 "C3" H 985 6204 50  0000 R CNN
F 1 "0.1uF" H 985 6295 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1138 6100 50  0001 C CNN
F 3 "~" H 1100 6250 50  0001 C CNN
	1    1100 6250
	-1   0    0    1   
$EndComp
Text Label 1950 6000 2    50   ~ 0
GND
Wire Wire Line
	1950 6000 2050 6000
Text Label 1950 5800 2    50   ~ 0
GND
Wire Wire Line
	1950 5800 2050 5800
Text Label 4150 5900 0    50   ~ 0
GND
Wire Wire Line
	4150 5900 4050 5900
Wire Wire Line
	2400 3550 2550 3550
Text Label 2400 3550 0    50   ~ 0
DTR
$Comp
L Device:C C4
U 1 1 5FC02CF0
P 2700 3550
F 0 "C4" V 2448 3550 50  0000 C CNN
F 1 "0.1uF" V 2539 3550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2738 3400 50  0001 C CNN
F 3 "~" H 2700 3550 50  0001 C CNN
	1    2700 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 4200 2950 3550
$Comp
L Device:R R1
U 1 1 5FC05D07
P 2950 3300
F 0 "R1" H 3020 3346 50  0000 L CNN
F 1 "10k" H 3020 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2880 3300 50  0001 C CNN
F 3 "~" H 2950 3300 50  0001 C CNN
	1    2950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3550 2950 3550
Wire Wire Line
	2950 3450 2950 3550
Connection ~ 2950 3550
Wire Wire Line
	2950 3050 2950 3100
Text Label 2950 3050 0    50   ~ 0
3.3V
Text Label 2950 3900 1    50   ~ 0
RESET
Text Label 1100 6500 0    50   ~ 0
GND
Wire Wire Line
	1100 6500 1100 6400
$Comp
L Device:C C5
U 1 1 5FC19E7B
P 4550 6150
F 0 "C5" H 4435 6104 50  0000 R CNN
F 1 "0.1uF" H 4435 6195 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4588 6000 50  0001 C CNN
F 3 "~" H 4550 6150 50  0001 C CNN
	1    4550 6150
	-1   0    0    1   
$EndComp
Text Label 4550 6400 0    50   ~ 0
GND
Wire Wire Line
	4550 6400 4550 6300
Wire Wire Line
	4050 6000 4550 6000
$Comp
L Device:R R2
U 1 1 5FC1CD53
P 3350 3300
F 0 "R2" H 3420 3346 50  0000 L CNN
F 1 "10k" H 3420 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3280 3300 50  0001 C CNN
F 3 "~" H 3350 3300 50  0001 C CNN
	1    3350 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5FC1EBC6
P 3700 3300
F 0 "R3" H 3770 3346 50  0000 L CNN
F 1 "10k" H 3770 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3630 3300 50  0001 C CNN
F 3 "~" H 3700 3300 50  0001 C CNN
	1    3700 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4200 3050 3650
Wire Wire Line
	3050 3650 3350 3650
Wire Wire Line
	3350 3650 3350 3450
Wire Wire Line
	3150 4200 3150 3700
Wire Wire Line
	3150 3700 3700 3700
Wire Wire Line
	3700 3700 3700 3450
Text Label 3700 3700 0    50   ~ 0
SDA
Text Label 3350 3650 0    50   ~ 0
SCL
Wire Wire Line
	2950 3100 3350 3100
Wire Wire Line
	3350 3100 3350 3150
Connection ~ 2950 3100
Wire Wire Line
	2950 3100 2950 3150
Wire Wire Line
	3350 3100 3700 3100
Wire Wire Line
	3700 3100 3700 3150
Connection ~ 3350 3100
Wire Wire Line
	6500 3200 6400 3200
Wire Wire Line
	6500 3100 6400 3100
Wire Wire Line
	2850 4100 2850 4200
Text Label 2850 4100 1    50   ~ 0
RXI
Wire Wire Line
	2750 4100 2750 4200
Text Label 2750 4100 1    50   ~ 0
TXO
Text Label 6500 3100 0    50   ~ 0
SDA
Text Label 6500 3200 0    50   ~ 0
SCL
$Comp
L Device:C C13
U 1 1 5FC3EE79
P 10500 3800
F 0 "C13" H 10385 3754 50  0000 R CNN
F 1 "0.01uF" H 10385 3845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10538 3650 50  0001 C CNN
F 3 "~" H 10500 3800 50  0001 C CNN
	1    10500 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	9900 3650 9950 3650
Text Label 10500 3650 0    50   ~ 0
3.3V
$Comp
L Device:R R4
U 1 1 5FC55D28
P 8400 3200
F 0 "R4" H 8470 3246 50  0000 L CNN
F 1 "10k" H 8470 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8330 3200 50  0001 C CNN
F 3 "~" H 8400 3200 50  0001 C CNN
	1    8400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3350 8400 3550
Wire Wire Line
	8400 3550 8600 3550
Wire Wire Line
	8400 2950 8400 3050
Text Label 8400 2950 0    50   ~ 0
3.3V
Text Label 9900 3750 0    50   ~ 0
GND
$Comp
L Device:C C11
U 1 1 5FC66156
P 9000 4750
F 0 "C11" H 8885 4704 50  0000 R CNN
F 1 "0.1uF" H 8885 4795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9038 4600 50  0001 C CNN
F 3 "~" H 9000 4750 50  0001 C CNN
	1    9000 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C7
U 1 1 5FC693D2
P 7850 4150
F 0 "C7" H 7965 4196 50  0000 L CNN
F 1 "0.1uF" H 7965 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7888 4000 50  0001 C CNN
F 3 "~" H 7850 4150 50  0001 C CNN
	1    7850 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 4900 9000 4900
Text Label 8850 4900 2    50   ~ 0
GND
$Comp
L Device:C C6
U 1 1 5FC6F949
P 7850 3750
F 0 "C6" H 7965 3796 50  0000 L CNN
F 1 "0.1uF" H 7965 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7888 3600 50  0001 C CNN
F 3 "~" H 7850 3750 50  0001 C CNN
	1    7850 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5FC7BDD5
P 9600 4750
F 0 "C12" H 9485 4704 50  0000 R CNN
F 1 "0.1uF" H 9485 4795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9638 4600 50  0001 C CNN
F 3 "~" H 9600 4750 50  0001 C CNN
	1    9600 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	9450 4900 9600 4900
Text Label 9450 4900 2    50   ~ 0
GND
Wire Wire Line
	8600 3650 8350 3650
Wire Wire Line
	8350 3650 8350 3600
Wire Wire Line
	8350 3600 7850 3600
Wire Wire Line
	8600 3850 8350 3850
Wire Wire Line
	8350 3850 8350 3900
Wire Wire Line
	8350 3900 7850 3900
Wire Wire Line
	8600 3950 7850 3950
Wire Wire Line
	7850 3950 7850 4000
Wire Wire Line
	8200 4050 8200 4300
Wire Wire Line
	8200 4300 7850 4300
Wire Wire Line
	8200 4050 8600 4050
Text Label 8600 4150 2    50   ~ 0
232_V-
Text Label 8600 3750 2    50   ~ 0
232_V+
Text Label 9000 4600 2    50   ~ 0
232_V+
Text Label 9600 4600 2    50   ~ 0
232_V-
Wire Wire Line
	9900 3550 9950 3550
Wire Wire Line
	9950 3550 9950 3650
Text Label 9900 3950 0    50   ~ 0
GND
$Comp
L Device:C C14
U 1 1 5FCBCEFC
P 10900 3800
F 0 "C14" V 10648 3800 50  0000 C CNN
F 1 "0.1uF" V 10739 3800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10938 3650 50  0001 C CNN
F 3 "~" H 10900 3800 50  0001 C CNN
	1    10900 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	9950 3650 10500 3650
Connection ~ 9950 3650
Wire Wire Line
	10500 3650 10900 3650
Connection ~ 10500 3650
Wire Wire Line
	9900 3950 10500 3950
Wire Wire Line
	10500 3950 10900 3950
Connection ~ 10500 3950
Text Label 9900 3850 0    50   ~ 0
TX_232
Text Label 8600 4250 2    50   ~ 0
RX_232
Text Label 9900 4250 0    50   ~ 0
RXI
Text Label 9900 4050 0    50   ~ 0
TXO
Text Notes 6950 1500 0    50   ~ 0
VIN = 4V to 36V max\n
$Comp
L Device:C C2
U 1 1 5FCE582E
P 5400 2050
F 0 "C2" H 5285 2004 50  0000 R CNN
F 1 "0.1uF" H 5285 2095 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5438 1900 50  0001 C CNN
F 3 "~" H 5400 2050 50  0001 C CNN
	1    5400 2050
	-1   0    0    1   
$EndComp
$Comp
L Device:C C1
U 1 1 5FCE62FD
P 4950 2050
F 0 "C1" H 4835 2004 50  0000 R CNN
F 1 "1uF" H 4835 2095 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4988 1900 50  0001 C CNN
F 3 "~" H 4950 2050 50  0001 C CNN
	1    4950 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4950 2200 5400 2200
Wire Wire Line
	5700 2200 5400 2200
Connection ~ 5400 2200
Wire Wire Line
	4950 1900 5400 1900
Wire Wire Line
	5800 1900 5400 1900
Connection ~ 5400 1900
Wire Wire Line
	5800 1900 5800 2200
Text Label 5650 1900 0    50   ~ 0
3.3V
Text Label 4950 2300 0    50   ~ 0
GND
Wire Wire Line
	4950 2300 4950 2200
Connection ~ 4950 2200
Wire Wire Line
	6500 2900 6400 2900
Text Label 6500 2900 0    50   ~ 0
3.3V
Text Label 6000 4000 0    50   ~ 0
GND
Wire Wire Line
	6000 4000 6000 3900
Wire Wire Line
	2950 7500 2950 7400
Wire Wire Line
	3050 7500 3050 7400
Text Label 2950 7500 3    50   ~ 0
SD
Text Label 3050 7500 3    50   ~ 0
INTB
Wire Wire Line
	5900 2100 5900 2200
Wire Wire Line
	6000 2100 6000 2200
Text Label 5900 2100 1    50   ~ 0
SD
Text Label 6000 2100 1    50   ~ 0
INTB
$Comp
L DSC6001C:DSC6001C Y1
U 1 1 5FD217CA
P 8700 5850
F 0 "Y1" H 9100 6115 50  0000 C CNN
F 1 "DSC6001C" H 9100 6024 50  0000 C CNN
F 2 "user-footprints:DSC6001C" H 9350 5950 50  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en587489" H 9350 5850 50  0001 L CNN
F 4 "Standard Clock Oscillators MEMS Oscillator, Ultra Low Power, LVCMOS, -40C-85C, 25ppm, 3.2x2.5mm" H 9350 5750 50  0001 L CNN "Description"
F 5 "" H 9350 5650 50  0001 L CNN "Height"
F 6 "Microchip" H 9350 5550 50  0001 L CNN "Manufacturer_Name"
F 7 "DSC6001CI2A-016.0000T" H 9350 5450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "DSC6001CI2A-016.0000T" H 9350 5350 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/dsc6001ci2a-016.0000t/microchip-technology" H 9350 5250 50  0001 L CNN "Arrow Price/Stock"
F 10 "DSC6001C" H 9350 5150 50  0001 L CNN "Mouser Part Number"
F 11 "DSC6001C" H 9350 5050 50  0001 L CNN "Mouser Price/Stock"
	1    8700 5850
	1    0    0    -1  
$EndComp
$Comp
L G17C0910132EU:G17C0910132EU J6
U 1 1 5FD2EF5D
P 10000 2850
F 0 "J6" H 11044 2896 50  0000 L CNN
F 1 "G17C0910132EU" H 11044 2805 50  0000 L CNN
F 2 "G17C0910132EU" H 10850 3150 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/18/amphenol_amphs14020-1-1733466.pdf" H 10850 3050 50  0001 L CNN
F 4 "D-Sub Standard Connectors D-SUB,PLUG,FP=8.08MM, 9-POS,R/A DIP,W/#4-40 IN" H 10850 2950 50  0001 L CNN "Description"
F 5 "12.93" H 10850 2850 50  0001 L CNN "Height"
F 6 "Amphenol" H 10850 2750 50  0001 L CNN "Manufacturer_Name"
F 7 "G17C0910132EU" H 10850 2650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 10850 2550 50  0001 L CNN "Arrow Part Number"
F 9 "" H 10850 2450 50  0001 L CNN "Arrow Price/Stock"
F 10 "G17C0910132EU" H 10850 2350 50  0001 L CNN "Mouser Part Number"
F 11 "G17C0910132EU" H 10850 2250 50  0001 L CNN "Mouser Price/Stock"
	1    10000 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 900  1500 900 
Text Label 1600 900  0    50   ~ 0
GND
Wire Wire Line
	5100 2900 5200 2900
Wire Wire Line
	5100 3000 5200 3000
Wire Wire Line
	5100 3100 5200 3100
Wire Wire Line
	5100 3200 5200 3200
Wire Wire Line
	5600 4000 5600 3900
Wire Wire Line
	5700 4000 5700 3900
Wire Wire Line
	5800 4000 5800 3900
Wire Wire Line
	5900 4000 5900 3900
Text Label 5100 2900 2    50   ~ 0
IN0A
Text Label 5100 3000 2    50   ~ 0
IN0B
Text Label 5100 3100 2    50   ~ 0
IN1A
Text Label 5100 3200 2    50   ~ 0
IN1B
Text Label 5600 4000 3    50   ~ 0
IN2A
Text Label 5700 4000 3    50   ~ 0
IN2B
Text Label 5800 4000 3    50   ~ 0
IN3A
Text Label 5900 4000 3    50   ~ 0
IN3B
Text Label 2600 2100 0    50   ~ 0
IN2A
Text Label 2600 2300 0    50   ~ 0
IN2B
Text Label 2600 2650 0    50   ~ 0
IN3A
Text Label 2600 2850 0    50   ~ 0
IN3B
Text Label 2600 1000 0    50   ~ 0
IN0A
Text Label 2600 1200 0    50   ~ 0
IN0B
Text Label 2600 1550 0    50   ~ 0
IN1A
Text Label 2600 1750 0    50   ~ 0
IN1B
Wire Wire Line
	1100 6100 2050 6100
Wire Wire Line
	9600 5950 9500 5950
Text Label 9600 5950 0    50   ~ 0
OSC
NoConn ~ 8700 5850
Text Label 9600 5850 0    50   ~ 0
3.3V
Text Label 8600 5950 2    50   ~ 0
GND
Wire Wire Line
	8700 5950 8600 5950
$Comp
L Device:C C15
U 1 1 5FDD5D3A
P 9900 6000
F 0 "C15" H 9785 5954 50  0000 R CNN
F 1 "0.1uF" H 9785 6045 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9938 5850 50  0001 C CNN
F 3 "~" H 9900 6000 50  0001 C CNN
	1    9900 6000
	-1   0    0    1   
$EndComp
Text Label 9900 6250 0    50   ~ 0
GND
Wire Wire Line
	9900 6150 9900 6250
Wire Wire Line
	9500 5850 9900 5850
Wire Wire Line
	6500 3000 6400 3000
Text Label 6500 3000 0    50   ~ 0
OSC
$Comp
L A-1JB:A-1JB IN0
U 1 1 5FDEFAC5
P 1450 700
F 0 "IN0" H 2100 650 50  0000 C CNN
F 1 "A-1JB" H 2150 750 50  0000 C CNN
F 2 "A-1JB" H 2700 1200 50  0001 L CNN
F 3 "https://www.amphenolrf.com/media/downloads/3960/CA-1JB.pdf" H 2700 1100 50  0001 L CNN
F 4 "AMC Straight PCB Jack, Surface Mount" H 2700 1000 50  0001 L CNN "Description"
F 5 "1.05" H 2700 900 50  0001 L CNN "Height"
F 6 "Amphenol" H 2700 800 50  0001 L CNN "Manufacturer_Name"
F 7 "A-1JB" H 2700 700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "A-1JB" H 2700 600 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/a-1jb/amphenol-rf" H 2700 500 50  0001 L CNN "Arrow Price/Stock"
F 10 "A-1JB" H 2700 400 50  0001 L CNN "Mouser Part Number"
F 11 "A-1JB" H 2700 300 50  0001 L CNN "Mouser Price/Stock"
	1    1450 700 
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 800  1500 800 
Wire Wire Line
	1500 800  1500 900 
Connection ~ 1500 900 
Wire Wire Line
	1500 900  1450 900 
Wire Wire Line
	1450 700  1500 700 
Wire Wire Line
	1500 700  1500 800 
Connection ~ 1500 800 
Wire Wire Line
	1950 6200 2050 6200
Text Label 1950 6200 2    50   ~ 0
OSC
$Comp
L Device:L_Small L2
U 1 1 5FE438C2
P 2000 1100
F 0 "L2" H 1956 1054 50  0000 R CNN
F 1 "1mH" H 1956 1145 50  0000 R CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 2000 1100 50  0001 C CNN
F 3 "~" H 2000 1100 50  0001 C CNN
	1    2000 1100
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 1000 1450 1000
Connection ~ 2000 1000
$Comp
L Device:C_Small C16
U 1 1 5FE858DE
P 2300 1100
F 0 "C16" H 2392 1146 50  0000 L CNN
F 1 "DNI" H 2392 1055 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 2300 1100 50  0001 C CNN
F 3 "~" H 2300 1100 50  0001 C CNN
	1    2300 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1000 2300 1000
Wire Wire Line
	2000 1200 2300 1200
Wire Wire Line
	2600 1000 2300 1000
Connection ~ 2300 1000
Wire Wire Line
	2600 1200 2300 1200
Connection ~ 2300 1200
Wire Wire Line
	1600 1450 1500 1450
Text Label 1600 1450 0    50   ~ 0
GND
$Comp
L A-1JB:A-1JB IN1
U 1 1 5FEFF78D
P 1450 1250
F 0 "IN1" H 2100 1200 50  0000 C CNN
F 1 "A-1JB" H 2150 1300 50  0000 C CNN
F 2 "A-1JB" H 2700 1750 50  0001 L CNN
F 3 "https://www.amphenolrf.com/media/downloads/3960/CA-1JB.pdf" H 2700 1650 50  0001 L CNN
F 4 "AMC Straight PCB Jack, Surface Mount" H 2700 1550 50  0001 L CNN "Description"
F 5 "1.05" H 2700 1450 50  0001 L CNN "Height"
F 6 "Amphenol" H 2700 1350 50  0001 L CNN "Manufacturer_Name"
F 7 "A-1JB" H 2700 1250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "A-1JB" H 2700 1150 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/a-1jb/amphenol-rf" H 2700 1050 50  0001 L CNN "Arrow Price/Stock"
F 10 "A-1JB" H 2700 950 50  0001 L CNN "Mouser Part Number"
F 11 "A-1JB" H 2700 850 50  0001 L CNN "Mouser Price/Stock"
	1    1450 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 1350 1500 1350
Wire Wire Line
	1500 1350 1500 1450
Connection ~ 1500 1450
Wire Wire Line
	1500 1450 1450 1450
Wire Wire Line
	1450 1250 1500 1250
Wire Wire Line
	1500 1250 1500 1350
Connection ~ 1500 1350
$Comp
L Device:L_Small L3
U 1 1 5FEFF79A
P 2000 1650
F 0 "L3" H 1956 1604 50  0000 R CNN
F 1 "1mH" H 1956 1695 50  0000 R CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 2000 1650 50  0001 C CNN
F 3 "~" H 2000 1650 50  0001 C CNN
	1    2000 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 1550 1450 1550
Connection ~ 2000 1550
$Comp
L Device:C_Small C17
U 1 1 5FEFF7A2
P 2300 1650
F 0 "C17" H 2392 1696 50  0000 L CNN
F 1 "DNI" H 2392 1605 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 2300 1650 50  0001 C CNN
F 3 "~" H 2300 1650 50  0001 C CNN
	1    2300 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1550 2300 1550
Wire Wire Line
	2000 1750 2300 1750
Wire Wire Line
	2600 1550 2300 1550
Connection ~ 2300 1550
Wire Wire Line
	2600 1750 2300 1750
Connection ~ 2300 1750
Wire Wire Line
	1600 2000 1500 2000
Text Label 1600 2000 0    50   ~ 0
GND
$Comp
L A-1JB:A-1JB IN2
U 1 1 5FF05483
P 1450 1800
F 0 "IN2" H 2100 1750 50  0000 C CNN
F 1 "A-1JB" H 2150 1850 50  0000 C CNN
F 2 "A-1JB" H 2700 2300 50  0001 L CNN
F 3 "https://www.amphenolrf.com/media/downloads/3960/CA-1JB.pdf" H 2700 2200 50  0001 L CNN
F 4 "AMC Straight PCB Jack, Surface Mount" H 2700 2100 50  0001 L CNN "Description"
F 5 "1.05" H 2700 2000 50  0001 L CNN "Height"
F 6 "Amphenol" H 2700 1900 50  0001 L CNN "Manufacturer_Name"
F 7 "A-1JB" H 2700 1800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "A-1JB" H 2700 1700 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/a-1jb/amphenol-rf" H 2700 1600 50  0001 L CNN "Arrow Price/Stock"
F 10 "A-1JB" H 2700 1500 50  0001 L CNN "Mouser Part Number"
F 11 "A-1JB" H 2700 1400 50  0001 L CNN "Mouser Price/Stock"
	1    1450 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 1900 1500 1900
Wire Wire Line
	1500 1900 1500 2000
Connection ~ 1500 2000
Wire Wire Line
	1500 2000 1450 2000
Wire Wire Line
	1450 1800 1500 1800
Wire Wire Line
	1500 1800 1500 1900
Connection ~ 1500 1900
$Comp
L Device:L_Small L4
U 1 1 5FF05490
P 2000 2200
F 0 "L4" H 1956 2154 50  0000 R CNN
F 1 "1mH" H 1956 2245 50  0000 R CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 2000 2200 50  0001 C CNN
F 3 "~" H 2000 2200 50  0001 C CNN
	1    2000 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 2100 1450 2100
Connection ~ 2000 2100
$Comp
L Device:C_Small C18
U 1 1 5FF05498
P 2300 2200
F 0 "C18" H 2392 2246 50  0000 L CNN
F 1 "DNI" H 2392 2155 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 2300 2200 50  0001 C CNN
F 3 "~" H 2300 2200 50  0001 C CNN
	1    2300 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2100 2300 2100
Wire Wire Line
	2000 2300 2300 2300
Wire Wire Line
	2600 2100 2300 2100
Connection ~ 2300 2100
Wire Wire Line
	2600 2300 2300 2300
Connection ~ 2300 2300
Wire Wire Line
	1600 2550 1500 2550
Text Label 1600 2550 0    50   ~ 0
GND
$Comp
L A-1JB:A-1JB IN3
U 1 1 5FF147DB
P 1450 2350
F 0 "IN3" H 2100 2300 50  0000 C CNN
F 1 "A-1JB" H 2150 2400 50  0000 C CNN
F 2 "A-1JB" H 2700 2850 50  0001 L CNN
F 3 "https://www.amphenolrf.com/media/downloads/3960/CA-1JB.pdf" H 2700 2750 50  0001 L CNN
F 4 "AMC Straight PCB Jack, Surface Mount" H 2700 2650 50  0001 L CNN "Description"
F 5 "1.05" H 2700 2550 50  0001 L CNN "Height"
F 6 "Amphenol" H 2700 2450 50  0001 L CNN "Manufacturer_Name"
F 7 "A-1JB" H 2700 2350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "A-1JB" H 2700 2250 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/a-1jb/amphenol-rf" H 2700 2150 50  0001 L CNN "Arrow Price/Stock"
F 10 "A-1JB" H 2700 2050 50  0001 L CNN "Mouser Part Number"
F 11 "A-1JB" H 2700 1950 50  0001 L CNN "Mouser Price/Stock"
	1    1450 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 2450 1500 2450
Wire Wire Line
	1500 2450 1500 2550
Connection ~ 1500 2550
Wire Wire Line
	1500 2550 1450 2550
Wire Wire Line
	1450 2350 1500 2350
Wire Wire Line
	1500 2350 1500 2450
Connection ~ 1500 2450
$Comp
L Device:L_Small L5
U 1 1 5FF147E8
P 2000 2750
F 0 "L5" H 1956 2704 50  0000 R CNN
F 1 "1mH" H 1956 2795 50  0000 R CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 2000 2750 50  0001 C CNN
F 3 "~" H 2000 2750 50  0001 C CNN
	1    2000 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 2650 1450 2650
Connection ~ 2000 2650
$Comp
L Device:C_Small C19
U 1 1 5FF147F0
P 2300 2750
F 0 "C19" H 2392 2796 50  0000 L CNN
F 1 "DNI" H 2392 2705 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 2300 2750 50  0001 C CNN
F 3 "~" H 2300 2750 50  0001 C CNN
	1    2300 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2650 2300 2650
Wire Wire Line
	2000 2850 2300 2850
Wire Wire Line
	2600 2650 2300 2650
Connection ~ 2300 2650
Wire Wire Line
	2600 2850 2300 2850
Connection ~ 2300 2850
$Comp
L Mechanical:MountingHole H1
U 1 1 5FF9D968
P 750 7500
F 0 "H1" H 850 7546 50  0000 L CNN
F 1 "MountingHole" H 850 7455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 750 7500 50  0001 C CNN
F 3 "~" H 750 7500 50  0001 C CNN
	1    750  7500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FF9F0C1
P 750 7300
F 0 "H2" H 850 7346 50  0000 L CNN
F 1 "MountingHole" H 850 7255 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 750 7300 50  0001 C CNN
F 3 "~" H 750 7300 50  0001 C CNN
	1    750  7300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5FEA9B76
P 1200 5100
F 0 "J1" H 1250 5417 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 1250 5326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 1200 5100 50  0001 C CNN
F 3 "~" H 1200 5100 50  0001 C CNN
	1    1200 5100
	1    0    0    -1  
$EndComp
Text Label 800  5000 0    50   ~ 0
MISO
Wire Wire Line
	1000 5000 800  5000
Text Label 800  5100 0    50   ~ 0
SCK
Wire Wire Line
	1000 5100 800  5100
Text Label 800  5200 0    50   ~ 0
RESET
Wire Wire Line
	1000 5200 800  5200
Text Label 1550 5000 0    50   ~ 0
3.3V
Wire Wire Line
	1500 5000 1700 5000
Text Label 1550 5100 0    50   ~ 0
MOSI
Wire Wire Line
	1500 5100 1700 5100
Text Label 1550 5200 0    50   ~ 0
GND
Wire Wire Line
	1500 5200 1700 5200
Text Label 3250 7500 3    50   ~ 0
MOSI
Wire Wire Line
	3250 7500 3250 7400
Wire Wire Line
	3350 7500 3350 7400
Text Label 3350 7500 3    50   ~ 0
MISO
Wire Wire Line
	4150 6300 4050 6300
Text Label 4150 6300 0    50   ~ 0
SCK
Text Label 6800 1250 0    50   ~ 0
3.3V
Wire Wire Line
	6800 1250 6950 1250
Text Label 10650 2350 0    50   ~ 0
RXI
Text Label 10650 2450 0    50   ~ 0
TXO
Wire Wire Line
	10500 2350 10650 2350
Wire Wire Line
	10500 2450 10650 2450
$EndSCHEMATC
