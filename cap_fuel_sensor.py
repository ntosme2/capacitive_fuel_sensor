#!/usr/bin/env python3
from math import pi, sqrt

d_air = 1
d_gasoline = 2
E = 8.854e-12

width = 0.02
length = 0.19
distance = 0.001 # 0.0017

area = width*length

L = 1e-3
Cload = 0 #10e-9

C0 = E * area / distance
Cempty = C0 * d_air
Cfull = C0 * d_gasoline

def fsense(C):
    return 1 / (2*pi * sqrt(L * C ) )
Fempty = fsense(C0 + Cempty)
Ffull = fsense(C0 + Cfull)

print('Fempty', int(Fempty))
print('Ffull', int(Ffull))
print('C empty:', Cempty)
print('C full:', Cfull)
